package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testF() {
        MinMax minMax = new MinMax();
        assertEquals(10, minMax.f(5, 10));
        assertEquals(5, minMax.f(5, 5));
        assertEquals(5, minMax.f(5, 3));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("Hello", minMax.bar("Hello"));
        assertEquals("", minMax.bar(""));
        assertEquals("", minMax.bar(null));
    }

}

