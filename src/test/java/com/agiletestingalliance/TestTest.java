package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestTest {

    @Test
    public void testGstr() {
        String str = "Hello, World!";
        com.agiletestingalliance.Test test = new com.agiletestingalliance.Test(str);
        assertEquals(str, test.gstr());
    }

}

